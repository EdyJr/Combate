package Connection;

import BoardPieces.BoardPiece;
import Interfaces.*;
import Pieces.Piece;
import Util.BoardPane;
import Util.Team;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.media.AudioClip;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Optional;

public class GameClient implements TouchManager, MessageSender, ObserverResult, ObserverOptions {

    private String host;
    private int port;
    private Boolean isPlayer1;
    private Team team;
    private Socket client;
    private ChatUpdater chatUpdater;
    private Ruler ruler;
    private Boolean withdrawal;
    private Boolean askRestart;
    private BoardReseter boardReseter;
    private Boolean alone;
    private Boolean exit;

    public Socket getClient() {
        return client;
    }

    public void setExit(Boolean exit) {
        this.exit = exit;
    }

    public Boolean getExit() {
        return exit;
    }

    public void setAlone(Boolean alone) {
        this.alone = alone;
    }

    public Boolean getAlone() {
        return alone;
    }

    public void setBoardReseter(BoardReseter boardReseter) {
        this.boardReseter = boardReseter;
    }

    public void setChatUpdater(ChatUpdater chatUpdater) {
        this.chatUpdater = chatUpdater;
    }

    public void setRuler(Ruler ruler) { this.ruler = ruler; }

    public void setWithdrawal(Boolean withdrawal) {
        this.withdrawal = withdrawal;
    }

    public void setAskRestart(Boolean askRestart) {
        this.askRestart = askRestart;
    }

    public GameClient (String host, int port) {

        this.host = host;
        this.port = port;
        this.withdrawal = false;
        this.askRestart = false;
        this.alone = true;
        this.exit = false;
    }

    public void execute() throws IOException {

        if (exit) { return; }
        client = new Socket(this.host, this.port);
        GameMessageReceiver r = new GameMessageReceiver(client.getInputStream(), this);
        new Thread(r).start();
    }

    //GAME

    public Team getTeam() { return team; }

    public void setPlayer1(Boolean player1) {

        isPlayer1 = player1;
        if (isPlayer1) { team = new Team("red"); }
        else { team = new Team("blue"); }
        team.setTouchManager(this);
        String msg = String.format("%10s %5s %s", "TEAM", team.color, team.gridToString());
        sendToServer(msg);
    }

    public void setTeamsOfRuler(String msg) {
        setAlone(false);
        ruler.receive(msg);
    }

    @Override
    public void touchBoard(int row, int col) {

        if (!withdrawal)
            ruler.touchBoard(team, row, col);
    }

    public void myTurn() {

        Task<Void> change = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    Thread.sleep(BoardPiece.reveal2Duration + BoardPiece.dieDuration + BoardPiece.moveDuration); }
                catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change.setOnSucceeded(event -> {
            team.getBoard().lock(false);
            ruler.updateTurn();
        });
        new Thread(change).start();
    }

    //MESSAGESENDER

    @Override
    public void send(String msg) {
        sendToServer(msg);
    }

    public void sendToChat(String name, String message) {

        try {
            chatUpdater.updateFeed(name, message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //OBSERVERRESULT INTERFACE

    @Override
    public void lockBoard() {
        team.getBoard().lock(true);
    }

    @Override
    public void sendToServer(String msg) {

        try {
            PrintStream ps = new PrintStream(client.getOutputStream());
            ps.println(msg);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void reveal(String name, int revRow, int revCol) {

        Piece p = Piece.getPieceBy(name, team.color == "red" ? "blue" : "red");
        team.grid[revRow][revCol].reveal(p);
    }

    @Override
    public void hide(int revRow, int revCol) {

        Task<Void> change = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    Thread.sleep(BoardPiece.dieDuration + BoardPiece.reveal2Duration); }
                catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change.setOnSucceeded(event -> {
            team.grid[revRow][revCol].unreveal();
        });
        new Thread(change).start();
    }

    @Override
    public void kill(int row, int col) {

        ruler.updateGrid(row, col);

        Task<Void> change2 = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    Thread.sleep(BoardPiece.dieDuration); }
                catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change2.setOnSucceeded(event -> {
            team.grid[row][col] = null;
        });

        Task<Void> change1 = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    Thread.sleep(2 * BoardPiece.reveal2Duration); }
                catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change1.setOnSucceeded(event -> {
            team.grid[row][col].die();
            new Thread(change2).start();
        });
        new Thread(change1).start();
    }

    @Override
    public void move(int oldRow, int oldCol, int newRow, int newCol, Boolean wait) {

        ruler.updateGrid(oldRow, oldCol, newRow, newCol);

        Task<Void> change2 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(BoardPiece.moveDuration);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change2.setOnSucceeded(event -> {
            team.grid[newRow][newCol] = team.grid[oldRow][oldCol];
            team.grid[oldRow][oldCol] = null;
        });

        final long duration = wait ? (2 * BoardPiece.reveal2Duration + BoardPiece.dieDuration) : 0;
        Task<Void> change1 = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(duration);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change1.setOnSucceeded(event -> {
            team.grid[oldRow][oldCol].moveTo(newRow, newCol, true);
            new Thread(change2).start();
        });
        new Thread(change1).start();
    }

    public void warnVictory() {

        Platform.runLater(() -> {
            setWithdrawal(true);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Victory");
            alert.setHeaderText("Your opponent was defeated!");
            String s = "What makes you the champion! Congratulations!";
            alert.setContentText(s);
            Optional<ButtonType> result = alert.showAndWait();

            if ((result.isPresent()) && (result.get() == ButtonType.OK))
                sendToServer("NOWRESTART");
            else
                sendToServer("NOWRESTART");
        });
    }

    public void warnDefeat() {

        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Defeated");
            alert.setHeaderText("Now you lose. Maybe next time...");
            String s = "Waiting a response from your opponent...";
            alert.setContentText(s);
            alert.show();
            setWithdrawal(true);
        });
    }

    @Override
    public void gameOver(String color) {

        if (color.compareTo(team.color) == 0)
            warnDefeat();
        else { warnVictory(); }
    }

    public void updatePos(int oldRow, int oldCol, int newRow, int newCol) {

        team.updatePos(oldRow, oldCol, newRow, newCol);
        ruler.updateGrid(oldRow, oldCol, newRow, newCol);
    }

    @Override
    public void warnInvalidMove() {

        String AUDIO_URL = getClass().getResource("/Audio/error.aiff").toString();
        AudioClip clip = new AudioClip(AUDIO_URL);
        clip.play();
    }

    //OBSERVEROPTIONS INTERFACE

    @Override
    public void giveUp() {

        if (!withdrawal) {
            Platform.runLater(() -> {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Withdrawal");
                alert.setHeaderText("Now you lose. Maybe next time...");
                String s = "Waiting a response from your opponent...";
                alert.setContentText(s);
                alert.show();
                setWithdrawal(true);
                sendToServer(String.format("%10s %5s", "GIVEUP", team.color));
            });
        } else { System.out.println("withdrawal on"); }
    }

    public void receiveWithdrawal() {

        Platform.runLater(() -> {
            setWithdrawal(true);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Withdrawal");
            alert.setHeaderText("Your opponent gave up!");
            String s = "What makes you the champion! Congratulations!";
            alert.setContentText(s);
            Optional<ButtonType> result = alert.showAndWait();

            if ((result.isPresent()) && (result.get() == ButtonType.OK))
                sendToServer("NOWRESTART");
            else
                sendToServer("NOWRESTART");
        });
    }

    @Override
    public void askRestart() {

        if (!askRestart) {
            Platform.runLater(() -> {
                setAskRestart(true);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Restart");
                alert.setHeaderText("A restart request was sent to your opponent");
                String s = "Wainting for confirmation...";
                alert.setContentText(s);
                alert.show();
                sendToServer(String.format("%10s %5s", "ASKRESTART", team.color));
            });
        } else { System.out.println("AskRestart on"); }
    }

    public void receiveRestartRequest(String teamName) {

        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Restart Request");
            String s = "Confirm to restart the match! (asked by " + teamName + ")";
            alert.setContentText(s);
            setAskRestart(true);

            Optional<ButtonType> result = alert.showAndWait();

            if ((result.isPresent()) && (result.get() == ButtonType.OK)) {
                sendToServer("NOWRESTART");
                setAskRestart(false);
            } else {
                sendToServer(String.format("%10s %5s", "RESRESULT", team.color));
                setAskRestart(false);
            }
        });
    }

    public void declineRestartRequest(String teamName) {

        setAskRestart(false);
        Platform.runLater(() -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Restart Request wasn't accepted");
            alert.setHeaderText(teamName + " has declined your restart request");
            String s = "Match must go on!";
            alert.setContentText(s);
            alert.show();
        });
    }

    public void restart() {

        if (exit) { return; }
        setWithdrawal(false);
        setAskRestart(false);
        boolean player1 = true;
        if (!alone) { player1 = team.color.compareTo("red") != 0; }
        BoardPane board = team.getBoard();
        boardReseter.removeBoard(board);
        team = null;
        setPlayer1(player1);
        if (alone) { boardReseter.restartGame(); }
    }

    public void exitGame() {
        sendToServer(String.format("%10s %5s", "REMOVE", team.color));
    }
}