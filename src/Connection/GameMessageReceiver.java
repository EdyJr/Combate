package Connection;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class GameMessageReceiver implements Runnable {

    private InputStream server;
    private GameClient gameClient;

    public GameMessageReceiver(InputStream server, GameClient gameClient) {

        this.server = server;
        this.gameClient = gameClient;
    }

    public void run() {

        Scanner s = new Scanner(this.server);
        Boolean flag = true;
        while (s.hasNextLine() && flag) {
            int oldRow, oldCol, newRow, newCol;
            String name, response = s.nextLine();
            String protocol = response.substring(0, 10).replaceAll(" ", "");
            switch (protocol) {
                case "WELCOME":
                    char color = response.charAt(11);
                    if (gameClient.getTeam() == null)
                        gameClient.setPlayer1(color == '1');
                    break;
                case "TEAM":
                    gameClient.setTeamsOfRuler(response);
                case "INVALID":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.warnInvalidMove();
                    break;
                case "MOVE":
                    oldRow = Integer.parseInt(response.substring(11, 12));
                    oldCol = Integer.parseInt(response.substring(13, 14));
                    newRow = Integer.parseInt(response.substring(15, 16));
                    newCol = Integer.parseInt(response.substring(17, 18));
                    name = response.substring(19);
                    name = name.replaceAll(" ", "");
                    gameClient.move(oldRow, oldCol, newRow, newCol, name.compareTo("YES") == 0);
                    break;
                case "KILL":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    oldRow = Integer.parseInt(response.substring(17, 18));
                    oldCol = Integer.parseInt(response.substring(19, 20));
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.kill(oldRow, oldCol);
                    break;
                case "REVEAL":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    String pieceName = response.substring(17, 27);
                    pieceName = pieceName.replaceAll(" ", "");
                    oldRow = Integer.parseInt(response.substring(28, 29));
                    oldCol = Integer.parseInt(response.substring(30, 31));
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.reveal(pieceName, oldRow, oldCol);
                    break;
                case "HIDE":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    oldRow = Integer.parseInt(response.substring(17, 18));
                    oldCol = Integer.parseInt(response.substring(19, 20));
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.hide(oldRow, oldCol);
                    break;
                case "UPDATE":
                    oldRow = Integer.parseInt(response.substring(11, 12));
                    oldCol = Integer.parseInt(response.substring(13, 14));
                    newRow = Integer.parseInt(response.substring(15, 16));
                    newCol = Integer.parseInt(response.substring(17, 18));
                    gameClient.updatePos(oldRow, oldCol, newRow, newCol);
                    break;
                case "PASS":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.lockBoard();
                    break;
                case "YOURS":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        gameClient.myTurn();
                    break;
                case "SENDER":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    int msgIndex = response.indexOf("TEXT") + 5;
                    String message = response.substring(msgIndex);
                    gameClient.sendToChat(name, message);
                    break;
                case "GIVEUP":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) != 0)
                        gameClient.receiveWithdrawal();
                    else {
                        if (gameClient.getExit())
                            gameClient.exitGame();
                    }
                    break;
                case "NOWRESTART":
                    gameClient.restart();
                    break;
                case "ASKRESTART":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) != 0)
                        gameClient.receiveRestartRequest(name);
                    break;
                case "RESRESULT":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) != 0)
                        gameClient.declineRestartRequest(name);
                    break;
                case "CLOSE":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) == 0) {
                        gameClient.setExit(true);
                        gameClient.giveUp();
                    } else { gameClient.setAlone(true); }
                    break;
                case "REMOVE":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) == 0)
                        flag = false;
                    break;
                case "GAMEOVER":
                    name = response.substring(11, 16);
                    name = name.replaceAll(" ", "");
                    if (name.compareTo(gameClient.getTeam().color) != 0)
                        gameClient.warnVictory();
                    else { gameClient.warnDefeat(); }
                    break;
            }
        }
        try {
            server.close();
            gameClient.getClient().close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
