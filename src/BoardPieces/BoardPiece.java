package BoardPieces;

import Pieces.*;
import Util.BoardPane;
import javafx.animation.ScaleTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.concurrent.Task;
import javafx.scene.Cursor;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class BoardPiece extends ImageView {

    public Piece piece;
    public static final long dieDuration = 600;
    public static final long moveDuration = 1400;
    public static final long reveal1Duration = 350;
    public static final long reveal2Duration = 700;

    public BoardPiece(Piece piece) {

        double unitWidth = BoardPane.defautWidth / BoardPane.columns;
        double unitHeight = BoardPane.defautHeight / BoardPane.rows;
        this.setFitWidth(unitWidth * 0.9);
        this.setFitHeight(unitHeight * 0.9);
        this.piece = piece;
        this.setSize();
        this.setCursor(Cursor.HAND);
    }

    public void chooseImage() {

        String color = piece.color;
        String imageName = "Images/Armies/" + color + "_" + piece.name + ".png";
        this.setImage(new Image(imageName));
    }

    private void setSize() {

        double width = 0.9 * BoardPane.defautWidth / BoardPane.columns;
        double height = 0.9 * BoardPane.defautHeight / BoardPane.rows;
        this.setFitWidth(width);
        this.setFitHeight(height);
    }

    public void setToolTip() {

        String text;
        if (piece.name != "Unknown") {
            if (this.piece.getClass().equals(Bomb.class))
                text = "defeats any attacking piece except 3";
            else if (this.piece.getClass().equals(Spy.class))
                text = "Can defeat the 10, but only if the 1 makes the attack";
            else if (this.piece.getClass().equals(Marshall.class))
                text = "Can be captured by the 1";
            else if (this.piece.getClass().equals(Miner.class))
                text = "Can defuse bombs";
            else if (this.piece.getClass().equals(Scout.class))
                text = "Can move any distance in a straight line, without leaping over pieces / lakes";
            else if (this.piece.getClass().equals(Flag.class))
                text = "its capture ends the game";
            else
                text = "VALUE = " + this.piece.value;
        } else
            text = ("No tip!");
        Tooltip t = new Tooltip(text);
        Tooltip.install(this, t);
    }

    public void moveTo(int row, int col, Boolean animated) {

        double []coord = getCoordinates(row, col);

        if (animated) {
            Transition sqt = getMoveTransition(row, col);
            sqt.play();
        } else {
            this.setTranslateX(coord[0]);
            this.setTranslateY(coord[1]);
        }
    }

    public void reveal(Piece secretPiece) {

        ScaleTransition st1 = new ScaleTransition(Duration.millis(reveal1Duration), this);
        st1.setToX(0);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(reveal1Duration), this);
        st2.setToX(1);

        SequentialTransition st = new SequentialTransition();
        st.getChildren().addAll(st1, st2);
        st.play();
        System.out.println("Reveal");

        Task<Void> change = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(reveal2Duration);
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
                return null;
            }
        };
        change.setOnSucceeded(event -> {
            this.piece = secretPiece;
            chooseImage();
        });
        new Thread(change).start();
    }

    public void unreveal() {
        reveal(new Unknown(this.piece.color));
    }

    public Transition getMoveTransition(int row, int col) {

        double []coord = getCoordinates(row, col);
        ScaleTransition st = new ScaleTransition(Duration.millis(300), this);
        st.setToX(1.5);
        st.setToY(1.5);

        TranslateTransition tt = new TranslateTransition(Duration.millis(800), this);
        tt.setToX(coord[0]);
        tt.setToY(coord[1]);

        ScaleTransition st2 = new ScaleTransition(Duration.millis(300), this);
        st2.setToX(1);
        st2.setToY(1);

        SequentialTransition sqt = new SequentialTransition();
        sqt.getChildren().addAll(st, tt, st2);
        return sqt;
    }

    public Transition getDieTransition() {

        ScaleTransition st = new ScaleTransition(Duration.millis(600), this);
        st.setToX(0);
        st.setToX(0);
        return st;
    }

    public void die() {

        Transition t = getDieTransition();
        t.play();
    }

    public double[] getCoordinates(int row, int col) {

        double unitWidth = BoardPane.defautWidth / BoardPane.columns;
        double unitHeight = BoardPane.defautHeight / BoardPane.rows;
        double xRel = unitWidth * ((double) col + 0.05);
        double yRel = unitHeight * ((double) row + 0.05);

        double []coord = new double[2];
        coord[0] = xRel;
        coord[1] = yRel;
        return coord;
    }
}
