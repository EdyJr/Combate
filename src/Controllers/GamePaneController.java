package Controllers;

import Interfaces.ObserverOptions;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class GamePaneController {

    @FXML public AnchorPane boardPane;
    @FXML public VBox chatPane;
    @FXML public Button giveUpButton;
    @FXML public Button restartButton;

    private ObserverOptions observerOptions;

    public void setObserverOptions(ObserverOptions observerOptions) {
        this.observerOptions = observerOptions;
    }

    @FXML private void giveUp() {

        System.out.println("Press Give Up!");
        observerOptions.giveUp();
    }

    @FXML private void restart() {

        System.out.println("Press Restart!");
        observerOptions.askRestart();
    }
}