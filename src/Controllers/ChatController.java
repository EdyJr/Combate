package Controllers;

import Interfaces.ChatUpdater;
import Interfaces.MessageSender;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import java.io.IOException;

public class ChatController implements ChatUpdater {

    @FXML
    private Button sendButton;
    @FXML private TextField textField;
    @FXML private VBox feed;
    @FXML private ScrollPane scrollPane;

    private String name;
    private MessageSender messageSender;

    public void setMessageSender(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public void setName(String name) { this.name = name; }

    public void sendMessage() {

        String message = textField.getText();
        if (!message.isEmpty()) {
            String sender = String.format("%10s %5s", "SENDER", name);
            message = String.format(" %5s %s", "TEXT", message);
            messageSender.send(sender + message);
        }
    }

    @FXML
    public void sendTouched() { sendMessage(); }

    @FXML
    public void textFieldAction (KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) { sendMessage(); }
    }

    @Override
    public void updateFeed(String sender, String message) throws IOException {

        boolean inLeft = true;
        if (sender.compareTo(name) == 0) {
            textField.setText("");
            inLeft = false;
        }
        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLs/RightMessage.fxml"));
        StackPane messageView = loader.load();
        MessageController controller = loader.getController();
        if (sender.compareTo(name) == 0) { sender = "Me"; }
        sender = sender.replace('b', 'B');
        sender = sender.replace('r', 'R');
        message = sender + ": " + message;
        controller.setMessageText(message);
        double ballonWidth = controller.image.getFitWidth();
        double offset1 = ballonWidth * 0.1;
        double offset2 = (scrollPane.getWidth() - ballonWidth) - offset1;
        controller.setX(inLeft ? offset1 : offset2);
        if (inLeft) { controller.setImage(new Image("Images/balloon_left.png")); }
        controller.setMessageColor(inLeft ? "ffffff" : "89C541", inLeft ? "000000" : "ffffff");
        Platform.runLater( () -> {
            feed.getChildren().add(messageView);
        });
    }
}
