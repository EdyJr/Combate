package Controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class MessageController {

    @FXML public TextArea message;
    @FXML public ImageView image;
    @FXML public StackPane stack;

    public void setMessageText(String text) {
        message.setText(text);
    }

    public void setMessageColor(String backgroundColor, String textColor) {
        message.setStyle("-fx-text-fill: #" + textColor + "; -fx-control-inner-background: #" + backgroundColor + ";");
    }

    public void setX (double x) {
        stack.setTranslateX(x);
    }

    public void setImage (Image image) {
        this.image.setImage(image);
    }
}
