package Controllers;

import Constants.ComparisonResult;
import Interfaces.GameView;
import Interfaces.Movable;
import Interfaces.ObserverResult;
import Interfaces.Ruler;
import Pieces.*;
import Util.Comparator;
import Util.Team;

import java.io.IOException;

public class GameController implements Ruler {

    private Team redTeam, blueTeam;
    public Piece[][] grid;
    public int turn = 1;
    public int winner = -1;
    private int touches = 0;
    private int oldRow, oldCol;
    private Piece first;
    private ObserverResult observerResult;
    private GameView gameView;

    public void setGameView(GameView gameView) {
        this.gameView = gameView;
    }

    public void setOr(ObserverResult or) {
        this.observerResult = or;
    }

    @Override
    public void receive(String formation) {

        redTeam = new Team("red");
        blueTeam = new Team("blue");
        int red = formation.indexOf("red");
        int blue = formation.indexOf("blue");
        String redFormation = formation.substring(red + 4, blue);
        String blueFormation = formation.substring(blue + 5);
        redTeam.setGridFrom(redFormation);
        blueTeam.setGridFrom(blueFormation);
        buildGrid(redFormation, blueFormation);
        turn = 1;
    }

    private void buildGrid(String redFormation, String blueFormation) {

        grid = new Piece[10][10];
        int count = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 10; j++) {
                String pieceName = blueFormation.substring(count, count + 10);
                pieceName = pieceName.replaceAll(" ", "");
                Piece piece = Piece.getPieceBy(pieceName, "blue");
                grid[i][j] = piece;
                count += 11;
            }
        }
        for (int i = 4; i < 6; i++)
            for (int j = 0; j < 10; j++)
                grid[i][j] = null;
        count = 0;
        for (int i = 6; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                String pieceName = redFormation.substring(count, count + 10);
                pieceName = pieceName.replaceAll(" ", "");
                Piece piece = Piece.getPieceBy(pieceName, "red");
                grid[i][j] = piece;
                count += 11;
            }
        }
        try {
            gameView.initGame();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean condition(Team t) {
        return (turn % 2 == 1 && t.color == "red") || (turn % 2 == 0 && t.color == "blue");
    }

    public Team currentTeam() {

        if (condition(redTeam)) { return redTeam; }
        return blueTeam;
    }

    public Team opponentTeam() {

        if (!condition(redTeam)) { return redTeam; }
        return blueTeam;
    }

    private boolean checkLose(String teamColor) {

        Boolean movable = false, flag = false;
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                Piece gij = grid[i][j];
                if (gij != null) {
                    String color = gij.color;
                    if (color.compareTo(teamColor) != 0) { continue; }
                    if (gij.name.compareTo("Flag") == 0) { flag = true; }
                    Class[] c = gij.getClass().getInterfaces();
                    if (c.length > 0 && c[0].equals(Movable.class))
                        movable = true;
                }
            }
        return !flag || !movable;
    }

    @Override
    public void updateGrid(int oldRow, int oldCol, int newRow, int newCol) {

        grid[newRow][newCol] = grid[oldRow][oldCol];
        grid[oldRow][oldCol] = null;
    }

    @Override
    public void updateGrid(int row, int col) {

        System.out.println("Remove " + row + " " + col + " from gameController");
        grid[row][col] = null;
    }

    @Override
    public void updateTurn() {

        turn++;
        if (checkLose("red")) {
            System.out.println("Red lost");
            String msg = String.format("%10s %5s", "GAMEOVER", "red");
            observerResult.sendToServer(msg);
            winner = 1;
            observerResult.lockBoard();
        } else if (checkLose("blue")) {
            System.out.println("Blue lost");
            String msg = String.format("%10s %5s", "GAMEOVER", "blue");
            observerResult.sendToServer(msg);
            winner = 2;
            observerResult.lockBoard();
        }
    }

    @Override
    public void touchBoard(Team team, int row, int col) {

        Team ct = currentTeam();
        Team ot = opponentTeam();
        if (ct.color.compareTo(team.color) != 0) {
            System.out.println("It's not your turn");
            observerResult.warnInvalidMove();
            return;
        }
        Piece touched = grid[row][col];
        if (touches == 0) {
            if (touched == null) {
                observerResult.warnInvalidMove();
                return;
            }
            if (touched.color.compareTo(ct.color) != 0) {
                System.out.println("Touch opponent's piece");
                observerResult.warnInvalidMove();
                return;
            }
            Class[] i = touched.getClass().getInterfaces();
            if (i.length == 0 || !i[0].equals(Movable.class)) {
                System.out.println("It doesn't move");
                System.out.println(touched.getClass().toString());
                observerResult.warnInvalidMove();
                return;
            }
            oldRow = row;
            oldCol = col;
            first = touched;
            touches++;
        } else {
            Movable pieceM;
            pieceM = (Movable) first;
            if (pieceM.checkMove(oldRow, oldCol, row, col)) {
                if (touched != null) {
                    if (touched.color.compareTo(first.color) != 0) {
                        String revealCmd1, hideCmd1, killCmd1, killCmd2, moveCmd1;
                        observerResult.lockBoard();
                        ComparisonResult cr = Comparator.comparePieces(first, touched);
                        System.out.println(cr);
                        switch (cr) {
                            case attacker_dies:
                                observerResult.reveal(touched.name, row, col);
                                observerResult.kill(oldRow, oldCol);
                                observerResult.hide(row, col);

                                String revealCmd = String.format("%10s %5s %10s %d %d", "REVEAL",
                                        ot.color, first.name, oldRow, oldCol);
                                String killCmd = String.format("%10s %5s %d %d", "KILL",
                                        ot.color, oldRow, oldCol);

                                observerResult.sendToServer(revealCmd);
                                observerResult.sendToServer(killCmd);
                                break;
                            case both_die:
                                observerResult.reveal(touched.name, row, col);
                                observerResult.kill(oldRow, oldCol);
                                observerResult.kill(row, col);

                                revealCmd1 = String.format("%10s %5s %10s %d %d", "REVEAL",
                                        ot.color, first.name, oldRow, oldCol);
                                killCmd1 = String.format("%10s %5s %d %d", "KILL",
                                        ot.color, oldRow, oldCol);
                                killCmd2 = String.format("%10s %5s %d %d", "KILL",
                                        ot.color, row, col);

                                observerResult.sendToServer(revealCmd1);
                                observerResult.sendToServer(killCmd1);
                                observerResult.sendToServer(killCmd2);
                                break;
                            case defender_dies:
                                observerResult.reveal(touched.name, row, col);
                                observerResult.kill(row, col);

                                revealCmd1 = String.format("%10s %5s %10s %d %d", "REVEAL",
                                        ot.color, first.name, oldRow, oldCol);
                                killCmd1 = String.format("%10s %5s %d %d", "KILL",
                                        ot.color, row, col);
                                hideCmd1 = String.format("%10s %5s %d %d", "HIDE",
                                        ot.color, oldRow, oldCol);
                                moveCmd1 = String.format("%10s %d %d %d %d %5s", "MOVE",
                                        oldRow, oldCol, row, col, "YES");

                                observerResult.sendToServer(revealCmd1);
                                observerResult.sendToServer(killCmd1);
                                observerResult.sendToServer(hideCmd1);
                                observerResult.sendToServer(moveCmd1);
                                break;
                            default:
                                System.out.println("Nobody dies");
                        }
                        turn++;
                        observerResult.sendToServer(String.format("%10s %5s", "YOURS", ot.color));
                    } else {
                        System.out.println("Same team");
                        observerResult.warnInvalidMove();
                    }
                } else {
                    System.out.println("Second touch empty");
                    observerResult.lockBoard();
                    turn++;
                    String moveCmd = String.format("%10s %d %d %d %d %5s", "MOVE", oldRow, oldCol, row, col, "NO");
                    String yoursCmd = String.format("%10s %5s", "YOURS", ot.color);
                    observerResult.sendToServer(moveCmd);
                    observerResult.sendToServer(yoursCmd);
                }
            } else {
                System.out.println("It doesn't reach or it is lake");
                observerResult.warnInvalidMove();
            }
            touches = 0;
        }
    }

    public void showGrid() {

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (grid[i][j] != null)
                    System.out.print(String.format("%10s ", grid[i][j].name + " "));
                else
                    System.out.print(String.format("__________ "));
            }
            System.out.println("\n");
        }
    }
}