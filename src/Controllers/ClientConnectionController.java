package Controllers;

import Connection.GameClient;
import Interfaces.BoardReseter;
import Interfaces.GameView;
import Util.BoardPane;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;
import sample.Main;
import java.io.IOException;

public class ClientConnectionController implements GameView, BoardReseter {

    @FXML private TextField host, port;
    @FXML private Button button;
    @FXML private Group group;
    @FXML private Label message, errorMessage;

    private GamePaneController controller;
    private GameClient gameClient;
    private Stage stage = null, stage2 = null;
    private String hostMemo, portMemo;
    private String defaultText = "Connecting to server...                   " +
            "A long wait time might mean a problem with the server or you " +
            "provided a wrong host or a wrong port";
    private String connectText = "Wainting another player connect to server...";

    @FXML
    private void startConnection() {

        if (host.getText().isEmpty() || port.getText().isEmpty()) {
            errorSound();
            return;
        }
        hostMemo = host.getText();
        portMemo = port.getText();
        group.setVisible(false);
        message.setText(defaultText);
        message.setVisible(true);
        errorMessage.setVisible(false);
        Task<Void> change = new Task<Void>() {
            @Override
            protected Void call() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        change.setOnSucceeded(event -> {
            if (gameClient == null) {
                gameClient = new GameClient(hostMemo, Integer.parseInt(portMemo));

                Platform.runLater(() -> {
                    try {
                        gameClient.execute();
                    } catch (IOException e) {
                        System.out.println("Connection problems");
                        errorSound();
                        group.setVisible(true);
                        message.setVisible(false);
                        errorMessage.setVisible(true);
                        gameClient = null;
                        return;
                    }
                });
            }
            GameController gameController = new GameController();
            gameController.setOr(gameClient);
            gameController.setGameView(this);
            gameClient.setRuler(gameController);
            gameClient.setBoardReseter(this);
            group.setVisible(false);
            message.setVisible(true);
            errorMessage.setVisible(false);
            message.setText(connectText);
        });
        new Thread(change).start();
    }

    @Override
    public void initGame() {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLs/GamePane.fxml"));
        controller = new GamePaneController();
        loader.setController(controller);
        controller.setObserverOptions(gameClient);

        FXMLLoader chatLoader = new FXMLLoader(getClass().getResource("FXMLs/Chat.fxml"));
        ChatController chatController = new ChatController();
        chatLoader.setController(chatController);
        VBox chatPane = new VBox();
        try {
            chatPane = chatLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        gameClient.setChatUpdater(chatController);
        chatController.setMessageSender(gameClient);

        SplitPane pane =  new SplitPane();
        try {
            pane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image emblem = new Image("Images/" + gameClient.getTeam().color + "Team.png");
        ImageView view = new ImageView(emblem);
        view.setFitWidth(300);
        view.setFitHeight(350);

        controller.boardPane.getChildren().add(gameClient.getTeam().getBoard());
        controller.chatPane.getChildren().addAll(chatPane, view);
        chatController.setName(gameClient.getTeam().color);

        SplitPane finalPane = pane;
        Platform.runLater(() -> {
            if (stage != null)
                stage.hide();
            stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Combate");
            stage.setScene(new Scene(finalPane, 910, 700));
            stage.show();
            stage.setOnCloseRequest(event -> {
                closeSocket();
            });
            button.getScene().getWindow().hide();
        });
    }

    private void errorSound() {

        String AUDIO_URL = getClass().getResource("/Audio/error.aiff").toString();
        AudioClip clip = new AudioClip(AUDIO_URL);
        clip.play();
    }

    @Override
    public void removeBoard(BoardPane board) {

        Platform.runLater(() -> {
            controller.boardPane.getChildren().remove(board);
        });
    }

    @Override
    public void restartGame() {

        Platform.runLater(() -> {
            stage.hide();
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("ClientConnection.fxml"));
            loader.setController(this);
            AnchorPane connection = null;
            try {
                connection = loader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            stage2 = new Stage();
            stage2.setResizable(false);
            stage2.setTitle("Client Connection");
            stage2.setScene(new Scene(connection, 535, 400));
            stage2.show();

            stage2.setOnCloseRequest(event -> {
                closeSocket();
            });

            host.setText(hostMemo);
            port.setText(portMemo);
            startConnection();
        });
    }

    public void closeSocket() {

        if (gameClient != null) {
            if (gameClient.getAlone() != null && gameClient.getAlone()) {
                gameClient.sendToServer(String.format("%10s %5s", "REMOVE", gameClient.getTeam().color));
                try {
                    gameClient.getClient().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else
                gameClient.sendToServer(String.format("%10s %5s", "CLOSE", gameClient.getTeam().color));
        }
    }
}