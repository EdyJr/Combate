package Constants;

public enum ComparisonResult { attacker_dies, defender_dies, both_die, nobody_dies }
