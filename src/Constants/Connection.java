package Constants;

public class Connection {

    public static final int messagePort = 12345;
    public static final int gamePort = 12346;
    public static final String host = "127.0.0.1";
}
