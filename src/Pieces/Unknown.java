package Pieces;

public class Unknown extends Piece {

    public Unknown (String color) {

        super(color);
        this.name = "Unknown";
        this.value = -1;
    }
}