package Pieces;

public abstract class Piece {

    public String name;
    public int value;
    public String color;

    public Piece() { }

    public Piece(String color) { this.color = color; }

    public static Piece getPieceBy(String name, String color) {

        switch (name) {
            case "Flag":
                return new Flag(color);
            case "Spy":
                return new Spy(color);
            case "Scout":
                return new Scout(color);
            case "Miner":
                return new Miner(color);
            case "Sergeant":
                return new Sergeant(color);
            case "Lieutenant":
                return new Lieutenant(color);
            case "Captain":
                return new Captain(color);
            case "Major":
                return new Major(color);
            case "Colonel":
                return new Colonel(color);
            case "General":
                return new General(color);
            case "Marshall":
                return new Marshall(color);
            case "Bomb":
                return new Bomb(color);
            default:
                return new Unknown(color);
        }
    }
}
