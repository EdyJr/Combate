package Pieces;

import Interfaces.Movable;
import Util.BoardPane;

public class Colonel extends Piece implements Movable {

    public Colonel (String color) {

        super(color);
        this.name = "Colonel";
        this.value = 8;
    }

    @Override
    public Boolean checkMove(int oldX, int oldY, int newX, int newY) {

        if (BoardPane.isInsideALake(newX, newY)) { return false; }
        int[] dx = { -1,  0, 1, 0 };
        int[] dy = {  0, -1, 0, 1 };

        for (int i = 0; i < 4; i++)
            if (newX == oldX + dx[i] && newY == oldY + dy[i])
                return true;
        return false;
    }
}