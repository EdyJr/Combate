package Pieces;

import Interfaces.Movable;
import Util.BoardPane;

public class Scout extends Piece implements Movable {

    public Scout(String color) {

        super(color);
        this.name = "Scout";
        this.value = 2;
    }

    @Override
    public Boolean checkMove(int oldX, int oldY, int newX, int newY) {

        if (BoardPane.isInsideALake(newX, newY)) { return false; }
        if ((newX != oldX && newY != oldY) || (newX == oldX && newY == oldY))
            return false;
        return true;
    }
}