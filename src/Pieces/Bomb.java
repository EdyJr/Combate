package Pieces;

public class Bomb extends Piece {

    public Bomb (String color) {

        super(color);
        this.name = "Bomb";
        this.value = 11;
    }
}