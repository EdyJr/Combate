package Pieces;

import Util.Team;

public class Flag extends Piece {

    public Flag (String color) {

        super(color);
        this.name = "Flag";
        this.value = 0;
    }
}