package sample;

import Controllers.ClientConnectionController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader(getClass().getResource("ClientConnection.fxml"));
        ClientConnectionController ccc = new ClientConnectionController();
        loader.setController(ccc);
        AnchorPane connection = loader.load();
        primaryStage.setResizable(false);
        primaryStage.setTitle("Client Connection");
        primaryStage.setScene(new Scene(connection, 535, 400));
        primaryStage.show();
        primaryStage.setOnCloseRequest(event -> {
            ccc.closeSocket();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
