package Interfaces;

public interface MessageSender {
    void send(String msg);
}
