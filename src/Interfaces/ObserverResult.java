package Interfaces;

import Pieces.Piece;

public interface ObserverResult {

    void sendToServer(String msg);
    void warnInvalidMove();
    void lockBoard();
    void reveal(String name, int revRow, int revCol);
    void hide(int revRow, int revCol);
    void kill(int row, int col);
    void move(int oldRow, int oldCol, int newRow, int newCol, Boolean wait);
    void gameOver(String color);
}
