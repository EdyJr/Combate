package Interfaces;

public interface Movable {
    Boolean checkMove(int oldX, int oldY, int newX, int newY);
}
