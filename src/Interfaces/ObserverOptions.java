package Interfaces;

public interface ObserverOptions {

    void giveUp();
    void askRestart();
}
