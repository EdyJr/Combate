package Interfaces;

import Util.Team;

public interface TouchManager {
    void touchBoard (int row, int col);
}