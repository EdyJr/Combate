package Interfaces;

import java.io.IOException;

public interface GameView {

    void initGame() throws IOException;
}
