package Interfaces;

import Util.Team;

public interface Ruler {

    void receive(String formation);
    void touchBoard(Team team, int row, int col);
    void updateTurn();
    void updateGrid(int oldRow, int oldCol, int newRow, int newCol);
    void updateGrid(int row, int col);
}
