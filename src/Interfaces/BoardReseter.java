package Interfaces;

import Util.BoardPane;

public interface BoardReseter {

    void removeBoard(BoardPane board);
    void restartGame();
}
