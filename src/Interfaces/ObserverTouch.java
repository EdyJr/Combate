package Interfaces;

public interface ObserverTouch {
    void warnManager(int row, int col);
}
