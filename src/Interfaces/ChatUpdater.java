package Interfaces;

import java.io.IOException;

public interface ChatUpdater {
    void updateFeed(String sender, String message) throws IOException;
}
