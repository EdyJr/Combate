package Util;

import BoardPieces.BoardPiece;
import Interfaces.ObserverTouch;
import Interfaces.TouchManager;
import Pieces.Piece;
import Pieces.Unknown;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.concurrent.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.min;

public class Team implements ObserverTouch {

    public String color;
    public Map<String, Integer> troops = new HashMap<String, Integer>();
    private BoardPane board;
    public BoardPiece[][] grid;
    private TouchManager touchManager;

    public Team(String color) {

        this.color  = color;
        setBoard(new BoardPane());
        setTroops();
        int minRow = 0, maxRow = 4, opponentMinRow = 6, opponentMaxRow = 10;
        if (color == "red") {
            minRow = 6;
            maxRow = 10;
            opponentMinRow = 0;
            opponentMaxRow = 4;
        }
        buildRandomFormation(minRow, maxRow);
        buildOpponentFormation(opponentMinRow, opponentMaxRow);
    }

    public void setTroops() {

        troops.put("Flag", 1);
        troops.put("Spy", 1);
        troops.put("Scout", 8);
        troops.put("Miner", 5);
        troops.put("Sergeant", 4);
        troops.put("Lieutenant", 4);
        troops.put("Captain", 4);
        troops.put("Major", 3);
        troops.put("Colonel", 2);
        troops.put("General", 1);
        troops.put("Marshall", 1);
        troops.put("Bomb", 6);
        troops.put("Unknown", 40);
    }

    public void setBoard(BoardPane board) {

        this.board = board;
        this.board.setObserver(this);
        this.board.setView();
    }

    public void setTouchManager(TouchManager touchManager) {
        this.touchManager = touchManager;
    }

    public BoardPane getBoard() { return board; }

    @Override
    public void warnManager(int row, int col) {
        touchManager.touchBoard(row, col);
    }

    public String gridToString() {

        String res = "";
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                BoardPiece bp = grid[i][j];
                if (bp == null || bp.piece.name == "Unknown") {
                    continue;
                }
                res += String.format("%10s ", bp.piece.name);
            }
        return res;
    }

    public void setGridFrom(String s) {

        grid = new BoardPiece[10][10];
        board.setObserver(this);
        board.setView();
        int count = 0;
        int minRow = color == "red" ? 6 : 0;
        int maxRow = color == "red" ? 10 : 4;
        for (int i = minRow; i < maxRow; i++)
            for (int j = 0; j < 10; j++) {
                String pieceName = s.substring(count, count + 10);
                pieceName = pieceName.replaceAll(" ", "");
                Piece piece = Piece.getPieceBy(pieceName, this.color);
                BoardPiece bp = new BoardPiece(piece);
                bp.chooseImage();
                bp.setToolTip();
                bp.moveTo(i, j,false);
                board.getChildren().add(bp);
                grid[i][j] = bp;
                count += 11;
            }
        int oMinRow = color == "red" ? 0 : 6;
        int oMaxRow = color == "red" ? 4 : 10;
        for (int i = oMinRow; i < oMaxRow; i++)
            for (int j = 0; j < 10; j++) {
                String opponent = color == "red" ? "blue" : "red";
                BoardPiece bp = new BoardPiece(new Unknown(opponent));
                bp.chooseImage();
                bp.setToolTip();
                bp.moveTo(i, j,false);
                board.getChildren().add(bp);
                grid[i][j] = bp;
            }
    }

    public void updatePos(int oldRow, int oldCol, int newRow, int newCol) {


    }

    private void buildRandomFormation(int minRow, int maxRow) {

        grid = new BoardPiece[BoardPane.rows][BoardPane.columns];
        ArrayList<IntegerPoint> rp = getRandomPositions(minRow, maxRow);
        int count = 0;

        for (String troop : troops.keySet())
            if (troop != "Unknown")
                count = this.fillWithClass(troop, color, rp, count);
    }

    private void buildOpponentFormation(int minRow, int maxRow) {

        String opponent = color == "red" ? "blue" : "red";
        ArrayList<IntegerPoint> pos = getPositions(minRow, maxRow);
        this.fillWithClass("Unknown", opponent, pos,0);
    }

    private ArrayList<IntegerPoint> getPositions(int minRow, int maxRow) {

        ArrayList<IntegerPoint> res = new ArrayList();
        for (int i = minRow; i < maxRow; i++)
            for (int j = 0; j < 10; j++) {
                IntegerPoint ip = new IntegerPoint(i, j);
                res.add(ip);
            }
        return res;
    }

    private ArrayList<IntegerPoint> getRandomPositions(int minRow, int maxRow) {

        ArrayList<IntegerPoint> res = getPositions(minRow, maxRow);
        Collections.shuffle(res);
        return res;
    }

    private int fillWithClass(String className, String owner, ArrayList<IntegerPoint> rp, int count) {

        int qtt = troops.get(className);
        for (int j = 0; j < qtt; j++) {
            Piece p = Piece.getPieceBy(className, owner);
            int x = rp.get(count).getRow();
            int y = rp.get(count).getColumn();
            BoardPiece bp = new BoardPiece(p);
            bp.moveTo(x, y,false);
            bp.chooseImage();
            bp.setToolTip();
            board.getChildren().add(bp);
            grid[x][y] = bp;
            count++;
        }
        return count;
    }
}