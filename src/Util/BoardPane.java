package Util;

import Interfaces.ObserverTouch;
import Interfaces.TouchManager;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class BoardPane extends AnchorPane {

    public static final int columns = 10;
    public static final int rows = 10;
    public static final double defautWidth = 600;
    public static final double defautHeight = 600;

    private Boolean isLocked;
    private ObserverTouch observer;

    public BoardPane() {

        super();

        this.setAction();
        this.setWidth(defautWidth);
        this.setHeight(defautHeight);
        this.isLocked = false;
    }

    public void setView() {

        ImageView view = new ImageView(new Image("/Images/grid.jpeg"));
        view.setFitWidth(defautWidth);
        view.setFitHeight(defautHeight);
//        Platform.runLater(() -> {
            this.getChildren().add(view);
//        });
    }

    public void setObserver(ObserverTouch observer) {
        this.observer = observer;
    }

    public void lock(Boolean value) { isLocked = value; }

    public void setAction() {

        this.setOnMouseClicked((MouseEvent event) -> {
            if (!isLocked) {
                double xTouch = event.getSceneX();
                double yTouch = event.getSceneY();

                double xRel = xTouch / defautWidth;
                double yRel = yTouch / defautHeight;
                int col = 0, row = 0, previous = 1;

                for (int i = 2; i <= columns; i++) {
                    if (xRel > (double) previous / columns && xRel <= (double) i / columns) { col = i - 1; }
                    if (yRel > (double) previous / rows && yRel <= (double) i / rows) { row = i - 1; }
                    previous = i;
                }
                observer.warnManager(row, col);
            } else { System.out.println("Locked BoardPane"); }
        });
    }

    public static boolean isInsideALake(int row, int col) {

        if (row >= 4 && row <= 5)
            if ((col >= 2 && col <= 3) || col >= 6 && col <= 7)
                return true;
        return false;
    }
}