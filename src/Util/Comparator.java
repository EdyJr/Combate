package Util;

import Constants.ComparisonResult;
import Pieces.Piece;

public class Comparator {

    //
    // @param  a = attacker Piece
    //         d = defenser Piece
    // @return result of the comparison between the pieces
    //
    public static ComparisonResult comparePieces(Piece a, Piece d) {

        if (a.color.compareTo(d.color) == 0) { return ComparisonResult.nobody_dies; }
        if (a.name == d.name) { return ComparisonResult.both_die; }
        if (d.name == "Bomb") {
            if (a.name == "Miner") { return ComparisonResult.defender_dies; }
            return ComparisonResult.both_die;
        }
        if (a.name == "Spy" && d.name == "Marshall"){ return ComparisonResult.defender_dies; }
        if (a.value > d.value) { return ComparisonResult.defender_dies; }
        return ComparisonResult.attacker_dies;
    }
}
