package Util;

public class IntegerPoint {

    private int row, col;

    public IntegerPoint() { this.row = this.col = 0; }

    public IntegerPoint(int row, int column) {

        this.row = row;
        this.col = column;
    }

    public void setRowandColumn(int row, int column) {

        this.row = row;
        this.col = column;
    }

    public int getRow() { return row; }

    public int getColumn() {return col; }
}
